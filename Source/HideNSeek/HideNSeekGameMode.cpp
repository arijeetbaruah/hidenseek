// Copyright Epic Games, Inc. All Rights Reserved.

#include "HideNSeekGameMode.h"
#include "HideNSeekCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHideNSeekGameMode::AHideNSeekGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
