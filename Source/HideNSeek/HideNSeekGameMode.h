// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HideNSeekGameMode.generated.h"

UCLASS(minimalapi)
class AHideNSeekGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHideNSeekGameMode();
};



