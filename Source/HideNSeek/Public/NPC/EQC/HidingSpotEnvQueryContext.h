// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "HidingSpotEnvQueryContext.generated.h"

/**
 * 
 */
UCLASS()
class HIDENSEEK_API UHidingSpotEnvQueryContext : public UEnvQueryContext
{
	GENERATED_BODY()

public:
		virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;
	
};
