// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC/AITask/BTT_FindRandomPatrol.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"
#include "AIController.h"


UBTT_FindRandomPatrol::UBTT_FindRandomPatrol(FObjectInitializer const& ObjectInitializer)
{
	NodeName = TEXT("Find Random Patrol");
}

EBTNodeResult::Type UBTT_FindRandomPatrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (AAIController* const cont = OwnerComp.GetAIOwner())
	{
		if (auto* npc = OwnerComp.GetOwner())
		{
			auto const origin = npc->GetActorLocation();
			if (auto* const NavSys = UNavigationSystemV1::GetCurrent(GetWorld()))
			{
				FNavLocation location;
				if (NavSys->GetRandomPointInNavigableRadius(origin, radius, location))
				{
					OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), location.Location);
					return EBTNodeResult::Succeeded;
				}
			}
		}
	}

	return EBTNodeResult::Failed;

}
