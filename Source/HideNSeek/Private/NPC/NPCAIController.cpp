// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC/NPCAIController.h"
#include "NPC/NPCCharacter.h"

void ANPCAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (ANPCCharacter* const npc = Cast<ANPCCharacter>(InPawn))
	{
		if (UBehaviorTree* const tree = npc->GetBehaviorTree())
		{
			UBlackboardComponent* b;
			UseBlackboard(tree->BlackboardAsset, b);
			Blackboard = b;
			RunBehaviorTree(tree);
		}
	}
}

UBlackboardComponent* ANPCAIController::GetBlackboardComponent() const
{
	return Blackboard;
}
