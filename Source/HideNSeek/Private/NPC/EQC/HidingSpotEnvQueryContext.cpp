// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC/EQC/HidingSpotEnvQueryContext.h"
#include "Kismet/GameplayStatics.h"
#include "HidableWall.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

void UHidingSpotEnvQueryContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHidableWall::StaticClass(), FoundActors);
	if (FoundActors.Num() != 0)
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, FoundActors);
	}
}
