// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC/EQC/PlayerEnvQueryContext.h"
#include "Kismet/GameplayStatics.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

void UPlayerEnvQueryContext::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	AActor* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (Player)
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, Player);
	}
}
