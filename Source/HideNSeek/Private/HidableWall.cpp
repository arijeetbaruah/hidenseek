// Fill out your copyright notice in the Description page of Project Settings.


#include "HidableWall.h"

// Sets default values
AHidableWall::AHidableWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHidableWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHidableWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

