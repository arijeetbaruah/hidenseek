// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class HideNSeek : ModuleRules
{
    public HideNSeek(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput" });
        PublicDependencyModuleNames.AddRange(new string[] { "AIModule", "NavigationSystem" });
    }
}
